#!/bin/python3

"""
Kasi's RFID module.

It is only an abstraction layer to simplify swapping out the RFID module/code. With the standard RC522 modules, this
does not do much.

Write your own driver class

DRIVER CLASS SPECIFICATION
methods:
+ read(timeout:int)
  returns the RFID transponder id or None. A timeout can be passed in seconds. Default is 0s (one immediate check).
+ __del__()
  clean up SPI channels etc.

"""

import time
import importlib.util

if importlib.util.find_spec("RPi"):
    import RPi.GPIO as GPIO
    DRIVER = "RC522"
else:
    DRIVER = "NoRFID"


class RFID:
    def __init__(self, language="en"):
        if DRIVER == "RC522":
            self.reader = RC522()
        else:
            self.reader = NoRFID()

    def read(self, timeout=0):
        id = self.reader.read(timeout)
        return id

    def __del__(self):
        del self.reader


class NoRFID:
    def __init__(self):
        pass

    def read(self, timeout):
        return None


class RC522:
    def __init__(self):
        from mfrc522 import SimpleMFRC522
        self.reader = SimpleMFRC522()

    def read(self, timeout):
        id: int = None
        # text: str = None
        start_time = time.time()
        id = self.reader.read_id_no_block()
        while time.time() - start_time < timeout and id is None:
            # id, text = self.reader.read_no_block()
            id = self.reader.read_id_no_block()
            time.sleep(0.1)
        return id

    def __del__(self):
        del self.reader
        GPIO.cleanup()


if __name__ == "__main__":
    print("RFID TEST")
    rfid = RFID()

    id = rfid.read(timeout=1)

    print(f"ID: {id}")

    from kasi_tts import TTS
    tts = TTS()

    #tts.say(text="New R-F-I-D transponder found", language="en", volume=1)
    #for c in str(id):
    #    tts.say(text=c, language="en", volume=1)

    if id is not None:
        tts.say(text="Neuer R-F-I-D-Transponder gefunden:", language="de", volume=1)
        for c in str(id):
            tts.say(text=c, language="de", volume=1)
            break

    del tts

