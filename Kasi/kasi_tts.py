#!/bin/python3

import sys
#from espeakng import ESpeakNG
import pyttsx3
import time

ENGINE = "ttsx3"


class TTS:
    def __init__(self, event_queue=None):
        if ENGINE == "espeak":
            pass
            #self.tts = TTS_espeakng()
        if ENGINE == "ttsx3":
            self.tts = TtsTtsx3()

    def say(self, text="", language="de", volume=1):
        self.tts.say(text, language, volume)

    def get_voices(self):
        return self.tts.get_voices()


class TtsTtsx3:
    def __init__(self):
        self.engine = pyttsx3.init()
        self.voices = {
            "de": {"voice": "german", "rate": 160},
            "en": {"voice": "english", "rate": 130},
            "fr": {"voice": "french", "rate": 130},
            "da": {"voice": "danish", "rate": 130},
        }

    def say(self, text="", language="de", volume=1, wait=True):
        if len(text) > 0:
            if language not in self.voices:
                language = "en"
            v = self.voices[language]
            self.engine.setProperty('voice', v["voice"])    # self.voices[language].id)
            self.engine.setProperty('rate', v["rate"])
            self.engine.setProperty('volume', volume)
            self.engine.say(text)
            if wait:
                self.engine.runAndWait()

    def get_voices(self):
        return self.engine.getProperty('voices')

    def get_voice_by_language(self, language):
        voices = self.get_voices()
        for i in range(0, len(voices)):
            if language in voices[i]:
                print("YES!")


class TtsEspeakng:
    def __init__(self):
        self.esng = ESpeakNG()
        self.voices = {
            "de": {"voice": "de",    "pitch": 50, "speed": 130},
            "en": {"voice": "en-gb", "pitch": 50, "speed": 130},
        }

    def say(self, text="", language="de", volume=1):
        v = self.voices[language]
        self.esng.voice = v["voice"]
        self.esng.pitch = v["pitch"]
        self.esng.say(text)

    def get_voices(self):
        return self.esng.voices


if __name__ == "__main__":
    interactive = False
    argv = sys.argv[1:]
    if len(argv) > 0:
        if argv.pop(0) == "-i":
            interactive = True

    tts = TTS()

    if interactive:
        do_loop = True
        language = "de"
        while do_loop:
            entry = input("> ")
            if entry.lower() == "q":
                do_loop = False
            elif entry.lower()[:2] == "l:":
                language = entry.lower()[2:]
            else:
                if len(entry) > 0:
                    tts.say(entry, language)
    else:
        for v in tts.get_voices():
            print(v)
        tts.say("The carnival of the animals", language="en")
        tts.say("Der Karneval der Tiere", language="de")
