#!/bin/python3
"""
####################################
   DOCPHEES' PYTHON LIBRARY (pdp)
####################################

This library holds often reused code functions and classes.
The library is released under the GPL license and can freely be reused.

Version and changes:
0.1.4
- getch can now print a prompt: getch(prompt="my prompt> ")
0.1.3
- cleaned up imports in Getch
- Getchd, the Getch daemon
- fix: Bug with stuck Return character in Getch/Getchd
0.1.2
- fix: renamed Getch instead of protected class _Getch
0.1.1
- added SignalCatcher and SignalBundle classes
    these allow simple, boolean handling of the SIGINT and SIGTERM signals
0.0.7
- added the iswin(), isosx() and islinux() functions.
- using is[os] functions in "clear"
0.0.6
- added clear, the ultimate clear screen function
0.0.5
- smaller _Getch class
0.0.4
initial release containing
- class _Getch
- class Version
- text cleaning
    removeHtmlTags()
    removeLeadingSpaces()
    cleanText()
- function printhist()
- function printtitle()
===TODOs================================
TODO considering moving tempest library into pdptools
     - depending on better maturity
TODO build a Getch based editor / input
"""

version = [0, 1, 4]
modulename = "pydpyl"

import sys, tty, termios
from select import select
import os
import traceback


# VERSION STRING HANDLING
class Version:
    """ The Version object stores and handles simple version numbering.

    object = pydpyl.Version(0,1,2)      sets version, subversion and minor version
    object([depth])                     returns the version as string
                                        (depth= 1: version, 2: ver+sub, 3: ver+sub+minor)
    object(num=True)                    returns the version as [ver, sub, minor], depth is ignored
    """

    def __init__(self, ver=0, sub=0, min=0):
        self.version = [int(ver), int(sub), int(min)]  # version, subversion and minor version

    def __call__(self, depth=3, num=False):  # calling the object returns the version as string
        versionstring = str(self.version[0])
        if not num:
            for n in range(1, depth):
                versionstring += "." + str(self.version[n])
            return versionstring
        else:
            return self.version


#######################################################################
# A non-blocking get-a-single-character-or-nothing from standard input
# The Getch class
#
# usage: import pydpyl
# getch = pydpyl.Getch(5) # the 5 is a 5 second timeout. you can use a 0 second timeout in a loop!
# input = getch()
# input now contains one character or nothing ("")
class Getch:
    """Gets a single character from standard input. Does not echo to the screen."""

    def __init__(self, to=1):
        self.timeout = to
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix(to)

    def __call__(self, timeout=None, prompt=None):
        return self.impl(timeout=timeout, prompt=prompt)


class _Getch(Getch):
    def __init__(self, timeout=None):
        print("[WARNING] The _Getch class is deprecated and will be removed soon.")
        print("          Please switch to Getch!")
        super().__init__(timeout)

    """Gets a single character from standard input. Does not echo to the screen."""
    # # # # # #  This legacy class will be removed in version 0.3
    """
    def __init__(self, to=1):
        self.timeout = to
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix(to)

    def __call__(self):
        return self.impl()
    """
    pass


class _GetchUnix:
    def __init__(self, to):
        self.timeout = to

    def __call__(self, timeout=None, prompt=None):
        if timeout is None:
            timeout = self.timeout
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        new_settings = old_settings[:]
        new_settings[3] = new_settings[3] & ~(termios.ECHO | termios.ICANON)
        ch = ''
        if prompt is not None:
            sys.stdout.write(prompt)
            sys.stdout.flush()
        try:
            # tty.setraw(sys.stdin.fileno())    # the return key got "stuck"
            termios.tcsetattr(fd, termios.TCSADRAIN, new_settings)
            [i, o, e] = select([sys.stdin.fileno()], [], [], timeout)
            if i:
                ch = sys.stdin.read(1)
            else:
                ch = ''
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt  # this will throw an error on non-windows machines, allowing for win/linux detection

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


#######################################################################
# A non-blocking get-a-single-character-or-nothing-from-standard-input daemon
# The Getchd class (uses the Getch class)
#
# usage: import pydpyl
# getchd = pydpyl.Getchd(5)   # the 5 is a 5 second timeout. you can use a 0 second timeout in a loop!
# input = getchd()
# input now contains one character or nothing ('')
# since getchd runs in the background, it makes sense to use getchd() in a while input != '': loop
# to empty the queue as quickly as possible.
# del getchd     # don't forget to clean up!

class Getchd:
    def __init__(self, ttl=0.5):
        self.ttl = ttl
        self.queue = None
        self.queue_back = None
        self.worker = self._create_worker()

    def _create_worker(self):
        from multiprocessing import Process, Queue
        self.queue = Queue()
        self.queue_back = Queue()
        fileno = sys.stdin.fileno()
        worker = Process(target=self._worker, args=(self.queue, self.queue_back, fileno,))
        worker.start()
        return worker

    def _worker(self, queue, queue_back, stdin_fileno):
        try:
            sys.stdin = os.fdopen(stdin_fileno)
            sig = SignalBundle([SignalCatcher(signal.SIGINT), SignalCatcher(signal.SIGTERM)])
            getch = Getch(self.ttl)
            alive = True
            while alive:
                char = getch()
                if char != '':
                    queue.put(char)
                if sig.getor():  # if sigint/sigterm are received
                    alive = False
                if not queue_back.empty():
                    if queue_back.get() == "kill":
                        self.queue.close()
                        self.queue_back.close()
                        alive = False
            del getch
        except:
            print("------- FATAL: Getchd._worker({0}, {1}, {2}) exited while multiprocessing.".format(queue, queue_back,
                                                                                                      stdin_fileno))
            traceback.print_exc()
            print("-------")

    def __call__(self, timeout=None):
        # timeout has no relevance here!
        received = ''
        if not self.queue.empty():
            received = self.queue.get()
            # print("not empty : " + str(received.encode('utf-8')) + "\r")
        return received

    def __del__(self):
        # del self.worker
        self.queue_back.put("kill")
        self.worker.join()


#######################################################################
# A simple non-blocking Signal Catcher for SIGTERM and SIGINT
#
# usage: import pydpyl
# sig_term = SignalCatcher(signal.SIG??) # create signal catcher
# alternatives:
# signals = SignalBundle( <SignalCatcher_object> )
# or
# signals = SignalBundle( [SignalCatcher(signal.SIGILL), SignalCatcher(signal.SIGTERM)])
# if signals.getor():
#     do_stuff()

import signal

class SignalCatcher:
    def __init__(self, sig):
        """
        Initialise SignalHandler object

        :param sig: the signal to be handle, example: signal.SIGTERM 
        """
        # for convenience by default it SIGTERM
        self.sig = sig
        self.caught = False
        signal.signal(sig, self._catch)

    def _catch(self, sig, frame):
        """
        Is run when the signal has been caught.

        :param sig:     the signal itself
        :param frame:   --
        :return: none
        """
        self.caught = True

    def get(self):
        """
        Check if the signal has been received since last get().
        This will reset(!) automatically to False.

        :return: boolean
        """
        returnval = self.caught
        self.caught = False
        return returnval

    def reset(self):
        """
        Simply reset the signal status.

        :return: none
        """
        void = self.get()


class SignalBundle:
    def __init__(self, sig=0):
        """
        Initialises the SignalBundle Object

        :param sig: a single or a list of SignalCatcher object(s)
        """
        self.sigs = list()
        if sig != 0:
            self.add(sig)

    def _sumandprod(self):
        """Create the sum and product of the objects' combined status (non-boolean!).
        Returns sum, product

        :param return: int, int
        """
        sig_sum = 0
        sig_prod = True
        for o in self.sigs:
            sig_stat = o.get()
            sig_sum += sig_stat
            sig_prod *= sig_stat
        return sig_sum, sig_prod

    def add(self, sig):
        """
        Add one or a list of SignalCatcher object(s) to the Bundle
        :param sig: <SignalCatcher> or list of <SignalCatcher> objects
        :return: none
        """
        if isinstance(sig, (list, tuple)):
            self.sigs += sig
        else:
            self.sigs.append(sig)

    def remove(self, sig):
        """
        Remove a SignalCatcher object from the bundle.

        Sig can be the SignalCatcher object itself or the index (int) of
        the object in the bundle.
        It returns the success of the deletion as boolean.

        :param sig: <SignalCatcher> or int
        :return: bool
        """

        removed = False
        try:
            self.sigs.remove(sig)
        except:
            if isinstance(sig, int):
                if len(self.sigs) >= int:
                    del self.sigs[sig]
                    removed = True
        else:
            removed = True
        return removed

    def getor(self):
        """
        Returns the OR combination of the bundle.
        RESETS THE STATUS!

        :return: boolean
        """
        sig_sum, void = self._sumandprod()
        if sig_sum > 0:
            return True
        else:
            return False

    def getand(self):
        """
        Returns the AND combination of the bundle.
        RESETS THE STATUS!

        :return: boolean
        """
        void, sig_prod = self._sumandprod()
        return bool(sig_prod)

    def getxor(self):
        """
        Returns the XOR combination of the bundle.
        RESETS THE STATUS!

        :return: boolean
        """
        sig_sum, void = self._sumandprod()
        if sig_sum == 1:
            returnval = True
        else:
            returnval = False
        return returnval

    def getorandxor(self):
        """
        Returns all three (OR, AND, XOR) combinations of the bundle.
        RESETS THE STATUS!

        :return: boolean, boolean, boolean
        """
        val_or = val_and = val_xor = False

        sig_sum, sig_prod = self._sumandprod()

        # OR
        if sig_sum == 0:
            val_or = True
        # AND
        val_and = bool(sig_prod)
        # XOR
        if sig_sum == 1:
            val_xor = True
        return val_or, val_and, val_xor


def signal_testsuite():
    import time
    print("TESTSUITE: SignalCatcher & SignalBundle")
    print(" setting up test")
    sig = SignalBundle([SignalCatcher(signal.SIGINT), SignalCatcher(signal.SIGTERM)])
    print(" looping. press CTRL+C to exit loop")
    n = 0
    while not sig.getor():
        n = n + 1
        time.sleep(0.1)
    print(" exited after", n, "loops.")
    print(" TEST finished")
    print("")


#################################################
#################################################
# FUNCTIONS
#################################################
#################################################

###################
# Is[OS]
# These functions return a boolean True if the os matches.

def iswin():
    import os
    if os.name in ('ce', 'nt', 'dos'):
        return True
    else:
        return False


def isosx():
    import os
    if os.name in ('osx'):
        return True
    else:
        return False


def islinux():
    import os
    if os.name in ('posix', 'linux'):
        return True
    else:
        return False


###################
# Clear
# The clear function works on any OS and chooses the appropriate method for
# a clear screen
def clear():
    import os
    # if os.name in ('ce','nt','dos'):
    if iswin():
        os.system('cls')
        return 'clear'  # returns the method used
    # elif os.name in ('posix','osx','linux'):
    elif islinux() or isosx():
        try:
            os.system('clear')
            return 'clear'  # returns the method used
        except:
            print('\n' * 120)
            return 'lines'  # returns the method used
    else:
        print('\n' * 120)
        return 'lines'  # returns the method used


###################
# Clean Strings
#
# These functions should be used with one-line strings
# If necessary use str.splitlines() and push the lines through the cleanText functions.
# This is very basic and so far does not provide any security at all
def removeHtmlTags(text):
    import re
    text = str(text)
    tag_re = re.compile(r'<[^>]+>')
    return tag_re.sub('', text)


def removeLeadingSpaces(text):
    text = str(text)
    for n in range(len(text)):
        if text[n] != " ":
            break
    return text[n:]


def cleanText(text):
    text = str(text)
    return removeLeadingSpaces(removeHtmlTags(text))


################
# PrintTitle
def printtitle(title=""):
    if str(title) != "":
        print(" .-" + len(str(title)) * "-" + "-.")
        print(" | " + str(title) + " |")
        print(" '-" + len(str(title)) * "-" + "-'")
    return 0


#################
# PrintHist()
def printhist(dataset, height=10, lowest="", highest="", columnw=3, title=""):
    """ Printhist prints a simple diagram/histogram of the
    values in a list, list of lbl/value pairs or dictionary.

    usage: printhist(dataset, height, min, max, columnwidth, title)

    dataset     any list of numbers
                or a dictionary
                or a list of lbl/data pairs like [ [1,5], ["hi",4] ]
    height      lines of the histogram (+1)
    lowest      manual selection of lowest value, must be integer!
    highest     manual selection of highest value, must be integer!
    columnw     width of each data point's column

    if lowest and highest are not set they are determined automatically.
    """

    # generate two lists, lbl and data, for label and data (number)
    lbl = []
    data = []

    if isinstance(dataset, list):
        # sanitise the dataset a bit and generate a label list, starting with 1 (!)
        if isinstance(dataset[0], list):
            for n in range(len(dataset)):
                lbl.append(str(dataset[n][0]))
                data.append(dataset[n][1])
        else:
            for n in range(len(dataset)):
                lbl.append(str(n + 1))
                if type(dataset[n]) in [float, int]:
                    data.append(dataset[n])
                else:
                    data.append(0)
    elif isinstance(dataset, dict):
        # split the dataset into a label list and a data list
        for n in dataset:
            lbl.append(str(n))
            if type(dataset[n]) in [float, int]:
                data.append(dataset[n])
            else:
                data.append(0)

    if lowest == "":
        lowest = min(data)
    if highest == "":
        highest = max(data)
    step = ((highest - lowest) / (height))

    # print a title for the graph
    printtitle(title)

    # create formatstring in style of "{:=7.2f}" for y-value formatting
    formatstring = "{:=" + str(max(len(str(int(highest))), len(str(int(lowest)))) + 4) + ".2f}"
    for n in range(height, -1, -1):
        linestr = ""
        for i in range(len(data)):
            if n * step + lowest <= data[i] < (n + 1) * step + lowest:
                # linestr += "*  "
                if i > 0:
                    if data[i - 1] < n * step + lowest:
                        linestr += ","
                    elif data[i - 1] >= (n + 1) * step + lowest:
                        linestr += "`"
                    else:
                        linestr += "-"
                else:
                    linestr += " "
                # if columnwidth > 2 then fill up with "-" characters
                linestr += (columnw - 2) * "-"
                # if columnwidth > 1, create a right border character for the graph
                if columnw > 1:
                    if i < len(data) - 1 and columnw > 1:
                        if data[i + 1] < n * step + lowest:
                            linestr += "."
                        elif data[i + 1] >= (n + 1) * step + lowest:
                            linestr += "'"
                        else:
                            linestr += "-"
                    else:
                        linestr += " "
            else:
                if i > 0:
                    if (data[i - 1] >= (n + 1) * step + lowest and data[i] < n * step + lowest) or (
                            data[i - 1] < n * step + lowest and data[i] >= (n + 1) * step + lowest):
                        linestr += "|"
                    else:
                        linestr += " "
                else:
                    linestr += " "
                linestr += (columnw - 1) * " "

        # now print the line
        print(formatstring.format((n * step + lowest)) + ": " + linestr)

    linestr = ""

    # find longest label(text)
    lbl_maxlen = 0
    for n in range(len(lbl)):
        lbl_maxlen = max(lbl_maxlen, len(str(lbl[n])))

    # create linestrings and print the label-lines
    for n in range(lbl_maxlen):
        linestr = (max(len(str(int(highest))), len(str(int(lowest)))) + 4 + 3) * " "
        for i in range(len(lbl)):
            if len(lbl[i]) > n:
                linestr += lbl[i][n]
            else:
                linestr += " "
            linestr += (columnw - 1) * " "
        print(linestr)


def printhist_testsuite():
    import math
    testname = "test 1"
    print("=====", testname, "=====")
    tmp = [2, 3, 1, -1, 0, 5, 6, 5, 3, 6.9]
    printhist(tmp, 10)
    printhist(tmp, 5, columnw=2)

    testname = "test 2"
    tmp = {}
    print("=====", testname, "=====")
    tmp = {0: 99892, 1: 100096, 2: 100086, 3: 100317, 4: 100044, 5: 99697, 6: 99962, 7: 99868, 8: 100064, 9: 99974,
           10: 10092, 11: 8988, 12: 9767, 13: 10037, 14: 9967, 15: 10062, 16: 9975, 17: 10070, 18: 10135, 19: 10104}
    printhist(tmp, 10)
    printhist(tmp, 20, 0, columnw=3)

    testname = "test 3"
    print("=====", testname, "=====")
    tmp = []
    z = 2 * math.pi
    while z < 6 * math.pi:
        tmp.append(['{:4.1f}'.format(z), math.sin(z)])
        z += ((6 * math.pi - 2 * math.pi) / 24)

    printhist(tmp, 10, columnw=2, title="sinus, 2 pi --> 4 pi")

    tmp = []
    z = 2 * math.pi
    while z < 6 * math.pi:
        tmp.append(['{:4.1f}'.format(z), math.sin(z)])
        z += ((6 * math.pi - 2 * math.pi) / 64)
    printhist(tmp, 8, columnw=1, title="sinus, 2 pi --> 4 pi")


######################################################
# simpleGetHtml
#
# gets the html-text from a valid url using urllib

def simpleGetHtml(url=""):
    import urllib.request
    url = str(url)
    html = ""
    if url != "":
        opener = urllib.request.FancyURLopener({})
        f = opener.open(url)
        html = f.read()
        return html
    else:
        return -1


###############################################
###############################################
# Running pydpyl
###############################################
###############################################
if __name__ == "__main__":
    ver = Version(version[0], version[1], version[2])
    print("pydpyl, version", ver(3))

    getch = _Getch(5)
    print("0 for all testsuites")
    print("q to quit")
    input = getch()
    if input == "0":
        printhist_testsuite()
        print("NEXT (Return)")
        input = getch()

        signal_testsuite()
        print("NEXT (Return)")
        input = getch()

    elif input in ["q", ""]:
        quit()
