#!/bin/python3

"""
Kasi daemon creates and manages the event queue for the Kasi music box player.
This includes:
- key/button events
- low power events

IO Rewrite Planning

methods:
start()
  start the io listener
get_event() -> event
  returns one single event of the queue
get_events() -> list(events)
  returns all new events since last call of get_event() or get_events()
clear_events()
  clears the event queue
stop()
  stops the io listener


for gpio pins (raspi):



for terminal mode:
  Use pydpyl.Getchd for getch in daemon mode. Then fork io out and give it a callback function (Kasi.on_event?)
  
  
Better Raspi-Check:
PinFactoryFallback: Falling back from native: unable to locate Pi revision in /proc/device-tree or /proc/cpuinfo

  
"""

import importlib.util
import sys
import os
import time
from multiprocessing import Process

DEBUG = True
PRETEND = False

if PRETEND:
    from gpiozero_sim import Button

    USE_GPIO = True
else:
    if importlib.util.find_spec("gpiozero"):
        from gpiozero import Button

        USE_GPIO = True
    else:
        USE_GPIO = False


class IO:
    def __init__(self, device, event_queue):
        self.device = device
        self.event_queue = event_queue
        self.io = []

        if USE_GPIO:
            self.io.append(GpioIO(event_queue))

        self.io.append(TerminalIO(event_queue))

    def __del__(self):
        for io in self.io:
            del io
        del self.io


class TerminalIO:
    def __init__(self, event_queue):
        self.events = []
        self.max_events = 50

        # Create listener, pipes, for process.
        # self.listener_conn, child_conn = Pipe()
        #         self.event_queue = Queue()
        self.event_queue = event_queue

        # give stdin to the worker / terminal io process
        stdin_fileno = sys.stdin.fileno()
        self.worker = Process(target=self.listener, args=(stdin_fileno,))
        self.worker.start()

    def listener(self, stdin_fileno):
        sys.stdin = os.fdopen(stdin_fileno)
        # print("Terminal Listener Started")
        import pydpyl

        getch = pydpyl.Getch(3)

        io_loop = True
        # print("Terminal Listener is ACTIVE")
        while io_loop:
            ch = getch()
            while ch != '':

                # create events
                if ch == "1":
                    self.event_queue.put("btn0")
                elif ch == "2":
                    if getch(0.5).lower() == "2":
                        self.event_queue.put("btn1_long")
                    else:
                        self.event_queue.put("btn1")
                elif ch == "3":
                    self.event_queue.put("btn2")
                elif ch == "4":
                    if getch(0.5).lower() == "4":
                        self.event_queue.put("btn3_long")
                    else:
                        self.event_queue.put("btn3")
                elif ch == "5":
                    self.event_queue.put("btn4")
                elif ch == "6":
                    self.event_queue.put("btn5")
                elif ch in ("q", "Q"):
                    self.event_queue.put("quit")
                ch = getch()

    def __del__(self):
        self.worker.terminate()
        self.worker.join()


class GpioIO:
    """IO driver for GPIO buttons from a Raspberry Pi

    This module uses the gpiozero library (or thhe gpiozero_sim library). It scans the button states
    and creates events from button presses.
    Currently it discerns between simple presses (below 1 second) and long presses (> 1 second).
    Buttons are scanned at 10Hz.
    While one button is held down. (First button wins.)
    """

    class NullDevice:
        def write(self, *args):
            pass

    def __init__(self, event_queue):
        self.event_queue = event_queue

        # send stdout to Nulldevice to suppress error messages
        original_stdout = sys.stdout
        sys.stdout = self.NullDevice()

        # TODO: put this into a GPIO.config file
        # Available RasPi-pins with RFID module RC522:
        # GPIO5, 6, 12, 13, 19, 16, 26, 20, 21      # GPIO name (GPIO#)
        # PIN 29 31 32  33  35  36  37  38  40      # Pin number
        pin_numbers = [5, 6, 12, 13, 19, 16, 26, 20, 21]
        # 29, 31, 32, 33, 35, 36, 37, 38, 40]
        self.buttons = []

        try:
            for pin_n in pin_numbers:
                print("[kasi_io] adding pin", pin_n)
                self.buttons.append(Button(pin_n))

        except Exception:  # TODO: proper exception handling
            print("[kasi_io] exception when setting up GPIO buttons")
            #sys.stdout = original_stdout
            self.worker = None
        else:
            self.worker = Process(target=self.listener, args=())
            self.worker.start()
        sys.stdout = original_stdout

    def listener(self):
        """
        https://gpiozero.readthedocs.io/en/v1.1.0/recipes.html

        :return:
        """
        io_loop = True
        while io_loop:
            event = None

            for n in range(0, len(self.buttons)):
                if self.buttons[n].is_pressed:
                    t = time.time()
                    while self.buttons[n].is_pressed:
                        time.sleep(0.1)
                    if time.time() - t < 1:
                        event = "btn{}".format(n)
                    else:
                        event = "btn{}_long".format(n)

                if event is not None:
                    self.event_queue.put(event)
                    break

            time.sleep(0.1)

    def __del__(self):
        if self.worker is not None:
            self.worker.terminate()
            self.worker.join()

    def read_raspi(self):
        pass


def dprint(*args):
    if DEBUG:
        out = ""
        for a in args:
            if len(out) > 0:
                out += " "
            out += str(a)
        print(out)


def main(argv):
    try:
        button = Button(2)
    except:
        PRETEND = True
    else:
        PRETEND = False
    if PRETEND:
        pass
    """
    # ALT  (< Kasi v0.2)
    while True:
        if button.is_pressed:
            print("Button is pressed")
        else:
            print("Button is not pressed")
        time.sleep(0.5)
    """
    exit()


if __name__ == "__main__":
    main(sys.argv[1:])
