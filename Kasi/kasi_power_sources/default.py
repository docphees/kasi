"""
The development / default kasi_power_sources source, currently a VARTA something...
"""

"""

@zidler@github When you just want to get the status of the LED you do not need to change anything!

just run: ./rpi3-gpiovirtbuf g 135
and as you earlier stated correctly: getting a zero means the LED is ON.
The led is "active low" that means when the signal is low (=0) it is active e.g. the led is on.

The meaning of the LED itself is inverses again: LED on means "NO power low".
So in the end: getting a zero with the virtbuf tool means ok and getting a 1 means low-power.

-> I just double checked with a good and a dodgy usb cable.

"""

class Powersource:
    def __init__(self):
        level = 1   # between 0 and 1

    def update_leve(self):
        """
        Updates the kasi_power_sources sources's kasi_power_sources level.
        """
        pass

    def get_level(self):
        """
        Returns the kasi_power_sources level of the kasi_power_sources source as a value between 0 and 1.
        :return: int
        """
        self.update_level()
        return self.level