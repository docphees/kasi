"""
Possibly implements power source detection.
Probably only detects low power mode by watching the raspi power led.


@zidler@github When you just want to get the status of the LED you do not need to change anything!

just run: ./rpi3-gpiovirtbuf g 135
and as you earlier stated correctly: getting a zero means the LED is ON.
The led is "active low" that means when the signal is low (=0) it is active e.g. the led is on.

The meaning of the LED itself is inverses again: LED on means "NO power low".
So in the end: getting a zero with the virtbuf tool means ok and getting a 1 means low-power.

-> I just double checked with a good and a dodgy usb cable.
"""

import os
import time
from multiprocessing import Process


class Power:
    def __init__(self, device, event_queue):
        self.event_queue = event_queue

        power_drivers = {"raspi": PowerRaspi,
                         "acpi_battery": PowerAcpiBattery,
                         "no_battery": PowerNoBattery,
                         None: PowerNoBattery}
        self.device = device
        # print("[kasi_power] device:", device)

        if self.device not in power_drivers:
            self.device = None

        self.power = power_drivers[self.device](self.event_queue)

    def __del__(self):
        del self.power


class PowerRaspi:
    """The journalctl has the two lines in the kernel unit:
    Under-voltage detected! (0x00050005)
    Voltage normalised (0x00000000)

    Maybe these can be used. Or is there some dbus way to do it?

    Careful: The low power trigger is passed through the VARTA power bank if it is being charged on a wonky cable!
    """
    def __init__(self, event_queue):
        self.event_queue = event_queue

        self.watcher = Process(target=self.watcher, args=())
        self.watcher.start()

    def watcher(self):
        power_loop = True
        while power_loop:
            if self.check_power():
                self.event_queue.put("low_power")
            time.sleep(30)

    def check_power(self):
        result = os.system('./rpi3-gpiovirtbuf g 135')   # TODO: This is not working for the raspi4
        return result == 0

    def __del__(self):
        self.watcher.terminate()
        self.watcher.join()


class PowerAcpiBattery:
    def __init__(self, event_queue):
        self.event_queue = event_queue

        self.watcher = Process(target=self.watcher, args=())
        self.watcher.start()

    def watcher(self):
        power_loop = True
        while power_loop:
            if self.check_power():
                self.event_queue.put("low_power")
            time.sleep(30)

    def check_power(self):
        low_power = False
        # result = result = os.system()
        with open('/sys/class/power_supply/BAT0/status', 'r') as f:
            status = f.read().lower().strip()
            # print("[kasi_power] status:", status, ".")
        if status in ('discharging',):
            with open('/sys/class/power_supply/BAT0/capacity', 'r') as f:
                capacity = f.read()
                # print("[kasi_power] capacity", float(capacity))
                if float(capacity) < 5:
                    low_power = True
        return low_power

    def __del__(self):
        self.watcher.terminate()
        self.watcher.join()


class PowerNoBattery:
    def __init__(self, event_queue):
        self.event_queue = event_queue


if __name__ == "__main__":
    pass
