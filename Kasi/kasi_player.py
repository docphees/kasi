#!/bin/python3

"""
Kaasi player of the Kasi music player.
either python-mpd2
https://python-mpd2.readthedocs.io/en/latest/topics/getting-started.html

PLAYER CLASS
+ __init__(event_queue)
  the player should place a "track_end" event into the event_queue
  vlc has its own event binding to set up in __init__, for other players you might have
  to write a non-blocking watcher script (i.e. via multiprocessing.Process or possibly asyncio)
+ get_state
  returns "play" or "pause"
+ set_track(track, position)
+ set_media(path)
+ set_position(position)
  position is a value between 0 and 1
+ play
+ pause
+ toggle_pause
  toggles between play and pause
+ stop
  stops the player (get_state still returns "pause")
+ get_length
  returns the duration of the current track
+ get_position
  returns the position as a value between 0 and 1
+ get_volume
  returns the current volume as value between 0 and 100
+ vol_up(dv)
  raises the volume by dv percent
+ vol_down(dv)
+ set_volume(vol)
  sets the volume to vol percent
+ __del__
  it is recommended to clean up watcher scripts etc.

"""

import sys
import time
import vlc
# from mpd import MPDClient


class VLCPlayer:
    def __init__(self, event_queue):
        self.event_queue = event_queue
        self.vlc_instance = vlc.Instance(["--no-video"])
        self.player = self.vlc_instance.media_player_new()
        self.media = None
        self.volume = self.player.audio_get_volume()

        events = self.player.event_manager()
        events.event_attach(vlc.EventType.MediaPlayerEndReached, self.__track_end)

    def get_state(self):
        state = self.player.get_state()
        if state == vlc.State.Playing:
            return "play"
        elif state in (vlc.State.Ended, vlc.State.Stopped, vlc.State.Paused):
            return "pause"

    def set_track(self, p_track, p_position=0):
        self.set_media(p_track.path)
        old_volume = self.get_volume()
        self.set_volume(0)
        self.play()
        time.sleep(0.1)
        self.pause()
        self.set_position(p_position)
        self.set_volume(old_volume)
        time.sleep(0.1)

    def set_media(self, p_path):
        self.media = self.vlc_instance.media_new(p_path)
        self.player.set_media(self.media)

    def set_position(self, p_position):
        if p_position < 0 or p_position > 1:
            p_position = 0
        vlc.libvlc_media_player_set_position(self.player, p_position)

    def play(self):
        self.player.play()
        return 0

    def pause(self):
        if self.get_state() != "pause":
            self.player.pause()
        return self.get_position()

    def toggle_pause(self):
        if self.get_state() != "pause":
            self.player.pause()
        else:
            self.player.play()

    def stop(self):
        self.player.stop()
        return self.get_position()

    def get_length(self):
        return round(vlc.libvlc_media_player_get_length(self.player) / 1000)

    def get_position(self):
        return vlc.libvlc_media_player_get_position(self.player)

    def get_volume(self):
        return self.player.audio_get_volume()

    def vol_up(self, dv=5):
        # keep vol between 0 and 100 (%)
        new_volume = max(0, min(100, self.get_volume() + dv))
        return self.set_volume(new_volume)

    def vol_down(self, dv=5):
        return self.vol_up(-dv)

    def set_volume(self, vol):
        self.player.audio_set_volume(vol)
        self.volume = self.get_volume()
        return self.volume

    def __track_end(self, event):
        self.event_queue.put("track_end")

    def __del__(self):
        self.player.stop()
        del self.player


def test(argv):
    """
    Test
    :param argv:
    :return:
    """
    from Kasi.kasi import Track
    import time
    player = VLCPlayer()
    track = Track(pPath="library/Fettes Brot lässt grüssen/03 Lieblingslied.mp3")
    player.set_track(track)
    player.play()
    player.pause()
    time.sleep(1)
    player.play()
    for i in range(0, 10):
        print(player.get_position_as_string())
        player.vol_up()
        time.sleep(1)
    for i in range(0, 10):
        print(player.get_position_as_string())
        player.vol_down()
        time.sleep(1)
    exit()


if __name__ == "__main__":
    test(sys.argv[1:])
