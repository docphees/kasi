#!python

"""
The File- / Album-manager component of the kasi project.

https://www.geeksforgeeks.org/flask-creating-first-simple-application/
https://github.com/stevelittlefish/flaskfilemanager/blob/master/README.md
http://exploreflask.com/en/latest/blueprints.html
"""

from flask import Flask
import flask
import flaskfilemanager
import time
from kasi import LIBRARY_PATH


def main():
    # Create the webapp
    app = Flask(__name__)

    # This is where the path for the uploads is defined
    app.config['FLASKFILEMANAGER_FILE_PATH'] = LIBRARY_PATH

    # You'll obviously do some more Flask stuff here!

    # Initialise the filemanager
    flaskfilemanager.init(app)

    @app.route("/")
    def filemanager():
        filemanager_link = flask.url_for('flaskfilemanager.index')
        # file_download_link = flask.url_for('flaskfilemanager.userfile', filename='/my_folder/uploaded_file.txt')
        return filemanager_link

    app.run()

    while True:
        time.sleep(1)


if __name__ == "__main__":
    main()