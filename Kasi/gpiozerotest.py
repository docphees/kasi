#!python3

from gpiozero import Button
import time
from pydpyl import Getch, clear

getch = Getch(1)

buttons = []

for i in range(22):
    buttons.append(Button(i))
    pass

loop = True
while loop:
    clear()
    print(f"---------------------- {time.time()}  (quit with 'q')'")
    for i in range(len(buttons)):
        print(f"Button {i}: {buttons[i].is_pressed}.")
    if getch() == "q":
        loop = False
