from importlib import util as importlib_util
import pip


def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])


imports = (
    ("gpiozero", "GPIO zero"),
    ("vlc", "python-vlc"),
    ("mpd", "python-mpd2"),
    ("espeakng", "pyespeakng"),
)

for imp in imports:
    if importlib_util.find_spec(imp[0]) is None:
        print(" [WARNING] Library {} ({}) is missing!".format(imp[0], imp[1]))
        print(f"--- TRYING TO INSTALL PACKAGE '{imp}'")
        install(imp)