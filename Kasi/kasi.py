#!python

"""
Kasi is an open source music box.

(c)2020 by Jan Rink.
"""

import sys
import os
from multiprocessing import Queue
import queue
from kasi_player import VLCPlayer as Player
from kasi_tts import TTS
from kasi_io import IO
from kasi_power import Power
from kasi_rfid import RFID
import time
import datetime

from pydpyl import clear

VERSION = "v0.6"
# These may be overwritten in kasi.conf
LIBRARY_PATH = "library/"
KASI_STATE_FILE = "kasi.state"
KASI_CONFIG_FILE = "kasi.conf"
ALBUM_INFOFILE = "000-album"
ALBUM_STATEFILE = "000-state"
DEBUG = True

# TODO: Decide on a Library location / config option


class Library:
    def __init__(self):
        self.albums = []

    def import_path(self, basepath):
        # first scan the tree from basepath on
        if os.path.isdir(basepath):
            for path in next(os.walk(basepath))[1]:
                album_path = basepath + path + "/"
                albumfile = album_path + ALBUM_INFOFILE
                statefile = album_path + ALBUM_STATEFILE  # can be used to write
                # TODO: statefile not used here...

                # get files in directory
                filelist = next(os.walk(album_path))[2]
                # dprint(filelist)
                filelist.sort()

                # keep only audio files "files"
                tracks = []
                for filename in filelist:
                    file_type: str
                    if any(file_type in filename.lower() for file_type in (".wav", ".mp3", ".ogg", ".flac")):
                        tracks.append(filename)
                # dprint(tracks)

                # if album has audio files, create album from them
                if len(tracks) > 0:
                    # directory contains audio files, so, lets make an album from it
                    rfid = []
                    tracknr = 0
                    album_name = path
                    language = "de"
                    position = 0
                    if os.path.isfile(albumfile):
                        with open(albumfile, "r") as f:
                            lines = f.readlines()
                        for line in lines:
                            line = line.strip()
                            key, value = self.breakup_config_line(line)
                            if key == "name":
                                album_name = value
                            if key == "language":
                                language = value
                            if key == "rfid":
                                rfid = value.replace(" ", "").split(",")
                            if key == "tracknr":
                                tracknr = value
                            if key == "position":
                                position = value

                    # TODO: Reading of the album needs to be done in Album class.
                    #       Only a path should be given to the Album.__init__()
                    new_album = Album(p_name=album_name, p_path=album_path, p_rfid=rfid,
                                      p_tracknr=tracknr, p_position=position, p_language=language)
                    # if os.path.isfile(albumfile):
                    new_album.write_album_info()

                    for t in tracks:
                        new_album.add_track(Track(p_album=new_album, p_path=album_path + t, p_name=t))
                    self.albums.append(new_album)


    @staticmethod
    def breakup_config_line(line):
        key, value = None, None
        line = line.strip()
        if "=" in line and line[0] != "#":
            key, value = line.split("=")
            key, value = key.strip(), value.strip()
        return key, value

    def get_next_album(self, p_album):
        i = self.albums.index(p_album) + 1
        i %= len(self.albums)
        return self.albums[i]

    def get_prev_album(self, p_album):
        i = self.albums.index(p_album) - 1
        i %= len(self.albums)
        return self.albums[i]

    def get_album_by_path(self, p_path):
        album = None
        for a in self.albums:
            if a.path == p_path:
                album = a
        # dprint("[info] found album", album.name)
        return album

    def get_album_by_name(self, p_name):
        album = None
        for a in self.albums:
            if a.name == p_name:
                album = a
        # dprint("[info] found album", album.name)
        return album

    def get_album_by_index(self, p_index):
        return self.albums[p_index]

    def get_album_by_rfid(self, id):
        album = None
        for a in self.albums:
            if id in a.rfid:
                album = a
        return album

    def clear(self):
        self.albums.clear()

    def is_empty(self):
        return len(self.albums) == 0


class Album:
    def __init__(self, p_name="", p_path="", p_rfid=[], p_tracks=[], p_tracknr=0, p_position=0, p_language="de"):
        self.name = p_name
        self.path = p_path
        self.rfid = p_rfid
        self.tracks = p_tracks[:]  # list of tracks
        self.language = p_language
        self.tracknr = p_tracknr  # last played track
        self.position = p_position  # track index / track position

    def add_track(self, p_track):
        self.tracks.append(p_track)

    def get_name(self):
        return self.name

    def get_language(self):
        return self.language

    def get_track_by_index(self, p_index):
        if p_index < 0 or p_index > len(self.tracks):
            p_index = 0
        return self.tracks[p_index]

    def get_tracknr(self, p_track):
        return self.tracks.index(p_track)

    def get_next_track(self, p_track):
        i = self.tracks.index(p_track) + 1
        i = i % len(self.tracks)
        return self.tracks[i]

    def get_prev_track(self, p_track):
        i = self.tracks.index(p_track) - 1
        return self.tracks[i]

    def is_last_track(self, p_track):
        return self.tracks.index(p_track) >= len(self.tracks) - 1

    def is_first_track(self, p_track):
        return self.tracks.index(p_track) == 0

    def write_album_info(self):
        with open(self.path + "/" + ALBUM_INFOFILE, "w") as f:
            f.write("name={}\n".format(self.name))
            f.write("path={}\n".format(self.path))
            f.write("rfid={}\n".format(','.join(self.rfid)))
            f.write("language={}\n".format(self.language))
            f.write("track={}\n".format(self.tracknr))
            f.write("position={}\n".format(self.position))

    def write_album_state(self, tracknr:int, position:float):
        # write the track and position to ALBUM_STATEFILE in the album path (self.path)
        with open(self.path + "/" + ALBUM_STATEFILE, "w") as f:
            f.write("tracknr={}\n".format(str(tracknr)))
            f.write("position={}\n".format(str(position)))

    def get_last_state(self):
        tracknr = 0
        position = 0
        if os.path.isfile(f"{self.path}/{ALBUM_STATEFILE}"):
            with open(self.path + "/" + ALBUM_STATEFILE, "r") as f:
                lines = f.readlines()
            for line in lines:
                key, value = breakup_config_line(line)
                if key == "tracknr":
                    tracknr = int(value)
                if key == "position":
                    position = float(value)
        return tracknr, position


    def __repr__(self):
        ret_str = "album[{}, {}, {}]".format(self.name, self.path, self.tracks)
        return ret_str


class Track:
    def __init__(self, p_album: Album = None, p_name="", p_path="", p_length=None, p_position=None):
        self.album = p_album
        self.name = p_name
        self.path = p_path
        #self.number = p_number
        self.length = p_length
        self.position = p_position


class Kasi:
    def __init__(self):
        self.load_config()

        self.on = False
        self.idle_since = time.time()
        self.device = self.detect_device()
        self.event_queue = Queue()
        self.library = Library()
        self.album = None  # current album
        self.track = None
        self.position = 0  # current position in track
        # TODO: DO NOT USE. Use player.position() instead!
        self.volume = 50
        self.welcome_message = "Kasi ist bereit!"
        self.welcome_language = "de"
        self.goodbye_message = "Tschühüß!"
        self.goodbye_language = "de"

        self.power = Power(self.device, self.event_queue)
        self.io = IO(self.device, self.event_queue)
        self.player = Player(self.event_queue)
        self.tts = TTS(self.event_queue)
        self.rfid = RFID()

    def detect_device(self):
        # possible devices:
        # raspi, acpi_battery, acpi_no_battery, None (unknown)
        # TODO: Write some device detection methods
        if os.path.isfile('/sys/class/power_supply/BAT0/status'):
            device = 'acpi_battery'
        else:
            device = 'no_battery'
        return device
        # TODO: Find out if Raspi
        #   device = 'raspi'

    def run(self):
        self.power_on()
        idle_since = time.time()

        while self.on:
            try:
                event = self.event_queue.get(timeout=1)   # blocking / waiting for events
            except queue.Empty:
                if self.player.get_state() == "play":
                    self.idle_since = time.time()
                elif time.time() - idle_since > 300:        # if idle for 5+ minutes
                    self.event_queue.put("sleep")
            else:
                # dprint("[Kasi] event received:", event)
                idle_since = time.time()    # Reset idle tracker
                self.on_event(event)

            clear()
            self.print_state(logo=True)

        del self.io
        del self.power
        del self.player

    def on_event(self, e):
        """
        Event handler
        :param e: String, event name
        :return: Bool, False if event not recognised
        """
        return_val = True
        if e == "btn0":
            self.power_off()
        elif e == "btn1":
            self.prev_track()
        elif e == "btn1_long":
            self.prev_album()
        elif e == "btn2":
            self.poll_rfid()
            self.toggle_pause()
        elif e == "btn3":
            self.next_track()
        elif e == "btn3_long":
            self.next_album()
        elif e == "btn4":
            self.vol_down()
        elif e == "btn5":
            self.vol_up()
        elif e == "track_end":
            self.next_track()
        elif e.lower() in ("quit", "low_power"):
            self.power_off()
        elif e.lower() in ("sleep",):
            self.power_off(silent=True)
        elif e.lower()[0:3] in ("rfid"):
            self.on_rfid(e[5:])
        else:
            return_val = False

        return return_val

    def poll_rfid(self):
        id = self.rfid.read(timeout=0)
        if id is not None:
            self.event_queue.put(f"rfid {id}")

    def on_rfid(self, id):
        rfid_album = self.library.get_album_by_rfid(str(id))
        if rfid_album is None:
            self.pause()
            self.tts.say("Neuer R-F-I-D-Transponder gefunden")
            for char in str(id):
                self.tts.say(char)
        elif rfid_album != self.album:
            self.pause()
            self.play_album(rfid_album)
        # print(f"[kasi] RFID-ID: {id}")

    def power_on(self):
        """
        This is run once when Kasi is started
        :return:
        """
        self.print_logo()
        self.load_config()
        # (re-)build library
        self.library.clear()
        self.library.import_path(LIBRARY_PATH)
        if self.library.is_empty():
            print("No media files have been found in location")
            print(LIBRARY_PATH)
            self.tts.say(text="No media files have been found at library location:", language="en", volume=1)
            self.tts.say(text=LIBRARY_PATH, language="en", volume=1)
        else:
            # TODO: Global variable has to be read from settings (libraries tuple)

            # read saved state
            self.load_state()
            # self.print_state()
            # check & find last medium/track/position
            # skip back 20 seconds or so if possible
            # prepare to play
            ########################
            self.player.set_track(self.track)
            self.player.set_position(self.position)
            self.player.pause()
            self.player.set_volume(self.volume)

            self.tts.say(self.welcome_message, self.welcome_language)
            self.player.play()

            self.on = True

    def power_off(self, silent=False):
        self.save_album_state()
        print("Kasi is shutting down...")
        if self.player.get_state() == "play":
            self.player.pause()
        if not silent:
            self.tts.say(self.goodbye_message, self.goodbye_language)
        self.save_state()
        self.on = False
        print("...done.")

    @staticmethod
    def print_logo():
        print(" ")
        print(" _________________")
        print("| KASI {:11}|".format(VERSION))
        print("|   ( )     ( )   |")
        print("|   ___________   |")
        print("|__/_o_______o_\\__|")
        print(" ")
        print("1:on/off       2:prev_track/_album")
        print("3:play/pause   4:next_track/_album")
        print("5:vol_down     6:vol_up")

    def play(self):
        self.player.play()

    def pause(self):
        self.player.pause()

    def toggle_pause(self):
        self.player.toggle_pause()

    def next_track(self):
        is_last = self.album.is_last_track(self.track)
        self.track = self.album.get_next_track(self.track)
        self.player.set_track(self.track)
        self.save_album_state()
        if not is_last:
            self.play()

    def prev_track(self):
        # if this is the first track of the album, prevent reverse wrap around.
        # also, only jump to prev. track if not further than 5 seconds into it.
        if self.album.is_first_track(self.track) or self.player.get_length() * self.player.get_position() > 4:
            self.pause()
            time.sleep(0.3)
            self.player.set_position(0)
            self.play()
        else:
            self.track = self.album.get_prev_track(self.track)
        self.player.set_track(self.track)
        self.save_album_state()
        self.play()

    def next_album(self):
        self.play_album(self.library.get_next_album(self.album))
        self.save_album_state()

    def prev_album(self):
        self.play_album(self.library.get_prev_album(self.album))
        self.save_album_state()

    def play_album(self, album: Album):
        self.pause()
        # save the album state before switching
        self.save_album_state()

        self.album = album
        time.sleep(0.3)
        self.tts.say(self.album.name, self.album.language, self.volume / 100)
        time.sleep(0.3)
        tracknr, position = self.album.get_last_state()
        self.track = self.album.get_track_by_index(tracknr)
        self.player.set_track(self.track)
        self.player.set_position(position)
        self.play()

    def save_album_state(self):
        tracknr = self.album.get_tracknr(self.track)
        position = self.player.get_position()
        if position * self.player.get_length() < 10:
            position = 0
        self.album.write_album_state(tracknr, position)

    def vol_down(self):
        self.player.vol_down()
        self.volume = self.player.get_volume()

    def vol_up(self):
        self.player.vol_up()
        self.volume = self.player.get_volume()

    def voice_over(self, text="", language="de", duck=False):
        """
        Currently un-used voice over
        :param text: str
        :param language: str
        :param duck: bool
        :return:
        """
        # This should duck playing sound out of the way for tts messages.
        # This method is currently not being used.
        self.tts.say(self.album.name, self.album.language, volume=self.volume / 100)

    def test(self):
        # time.sleep(1)
        for i in range(0, 2):
            print(self.player.get_position())
            time.sleep(1)
        self.next_track()
        for i in range(0, 5):
            print(self.player.get_position())
            time.sleep(1)
        self.next_album()
        self.next_track()
        self.next_track()
        for i in range(0, 5):
            print(self.player.get_position())
            time.sleep(1)
        self.prev_album()
        self.next_track()
        self.next_track()
        self.next_track()
        self.next_track()
        for i in range(0, 5):
            print(self.player.get_position())
            time.sleep(1)

        self.power_off()

    def load_config(self):
        global LIBRARY_PATH, KASI_STATE_FILE, KASI_CONFIG_FILE, ALBUM_INFOFILE, ALBUM_STATEFILE, DEBUG
        if os.path.isfile(KASI_CONFIG_FILE):
            with open(KASI_CONFIG_FILE, "r") as f:
                lines = f.readlines()
            for line in lines:
                key, value = self.breakup_config_line(line)
                if key == "LIBRARY_PATH":
                    LIBRARY_PATH = value
                if key == "KASI_STATE_FILE":
                    KASI_STATE_FILE = value
                if key == "KASI_CONFIG_FILE":
                    KASI_CONFIG_FILE = value
                if key == "ALBUM_INFOFILE":
                    ALBUM_INFOFILE = value
                if key == "ALBUM_STATEFILE":
                    ALBUM_STATEFILE = value
                if key == "DEBUG":
                    try:
                        DEBUG = bool(value)
                    except EXCEPTION:
                        DEBUG = False

        # TODO: write load_config method. (Kasi.conf)

    def load_state(self):
        if os.path.isfile(KASI_STATE_FILE):
            tracknr = 0
            position = 0
            with open(KASI_STATE_FILE, "r") as f:
                lines = f.readlines()
            for line in lines:
                key, value = self.breakup_config_line(line)
                if key == "album":
                    self.album = self.library.get_album_by_name(value)
                if key == "albumpath":
                    self.album = self.library.get_album_by_path(value)
                if key == "volume":
                    try:
                        self.volume = int(value)
                    except Exception:
                        self.volume = 50
                if key == "welcome_message":
                    self.welcome_message = value
                if key == "welcome_language":
                    self.welcome_language = value
                if key == "goodbye_message":
                    self.goodbye_message = value
                if key == "goodbye_language":
                    self.goodbye_language = value

        if len(self.library.albums) > 0:
            if self.album is None:
                self.album = self.library.get_album_by_index(0)
                tracknr = 0
                self.position = 0
            else:
                tracknr, self.position = self.album.get_last_state()
            self.track = self.album.get_track_by_index(tracknr)

    @staticmethod
    def cleanup_line(line, option):
        return line.replace(option + "=", "").strip()

    @staticmethod
    def breakup_config_line(line):
        key, value = None, None
        line = line.strip()
        if "=" in line and line[0] != "#":
            key, value = line.split("=")
            key, value = key.strip(), value.strip()
        return key, value

    def print_state(self, logo=False):
        self.volume = self.player.get_volume()
        if logo:
            self.print_logo()
        print(f" track: {self.track.name}")
        print(f" album: {self.album.name}")
        if self.player.get_state() == "play":
            state = "▶"
        else:
            state = "⏸"
        print(f" {state} " +
              f"{str(datetime.timedelta(seconds=int(self.player.get_position() * self.player.get_length())))} " +
              f"[{(int(self.player.get_position() * 31)+1)*'=':30}]   ")
        if self.volume < 33:
            volstring = "🔈"
        elif self.volume < 66:
            volstring = "🔉"
        else:
            volstring = "🔊"
        # volstring = (int(self.volume / 33)) * ')'
        print(f" {volstring} {self.volume:3}%")

    def save_state(self):
        # if os.path.isfile(KASI_STATE_FILE):
        with open(KASI_STATE_FILE, "w") as f:
            if self.album is not None:
                f.write("album={}\n".format(self.album.name))
                f.write("albumpath={}\n".format(self.album.path))
                f.write("volume={}\n".format(self.volume))
                f.write("welcome_message={}\n".format(self.welcome_message))
                f.write("welcome_language={}\n".format(self.welcome_language))
                f.write("goodbye_message={}\n".format(self.goodbye_message))
                f.write("goodbye_language={}\n".format(self.goodbye_language))

    def __del__(self):
        pass
        self.event_queue.close()


def dprint(*args):
    if DEBUG:
        out = ""
        for a in args:
            if len(out) > 0:
                out += " "
            out += str(a)
        print(out)

#@staticmethod
def breakup_config_line(line):
    key, value = None, None
    line = line.strip()
    if "=" in line and line[0] != "#":
        key, value = line.split("=")
        key, value = key.strip(), value.strip()
    return key, value


def main(argv):
    """
    Kasi's main function.
    :param argv:
    :return: None
    """
    kasi = Kasi()
    kasi.run()
    # Kasi.test()


if __name__ == "__main__":
    main(sys.argv[1:])
