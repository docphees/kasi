from multiprocessing import Queue, Process
import time

"""
This module simulates Buttons to replace the gpiozero library when no Raspberry Pi is available for testing.

It reads the file gpiozero_sim.program and interpretes the lines.

Commands can be:
p x   pause x seconds (x may be float)
d x   simulate button x pressed "d"own
u x   simulate button x released ("u"p)

sample program:
----------------------
p 1.5
d 3
p 0.5
u 3
p 2
----------------------
This program waits for 1.5 seconds, simulates button 3 as pressed down for half a second, releases button 3 and 
pauses for another 2 seconds.
"""


class Button:
    """
    Simulating a GPIO Button for development
    """
    def __init__(self, number):
        self._n = number
        self._state = False
        with open("gpiozero_sim.program", "r") as f:
            raw = f.readlines()
        self.p = []
        for line in raw:
            self.p.append(line.strip().split(" "))

        self._queue = Queue()
        self._worker = Process(target=self.__reader, args=())
        self._worker.start()

    def __reader(self):
        for p in self.p:
            if p[0] == "p":
                time.sleep(float(p[1]))
            elif p[0] == "d" and p[1] == str(self._n):
                self._queue.put(True)
            elif p[0] == "u" and p[1] == str(self._n):
                self._queue.put(False)
            else:
                pass

    def is_pressed(self):
        while not self._queue.empty():
            self._state = self._queue.get()
        return self._state

    def __del__(self):
        self._worker.terminate()
        self._worker.join()
        self._queue.close()


if __name__ == "__main__":
    b = Button(3)
    for i in range(100):
        print("b.is_pressed()=", b.is_pressed())
        time.sleep(0.1)
    del b
