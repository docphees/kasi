![# KASI](project_resources/kasi_logo_banner.png "Kasi Logo")


A musicbox for children, bringing back the ease of use of cassette players.
The name "Kasi" is a german short for "Kassettenrecorder", "cassette player" in 
english.

Kasi is written in python for the Raspberry Pi, but follows a modular approach,
which should make it simple to adapt it do different hardware.

The software is modular to allow simple replacement of the media player 
(kasi_player), the power watchdog (kasi_power) and the control elements 
(kasi_io).


# Installation

## Install Raspbian

Install Raspbian from www.raspberrypi.org (https://www.raspberrypi.org/downloads/raspberry-pi-os/). I use the lite version without a desktop to keep the system lean, but you might
include the desktop. The KASI becomes a small, portable computer that way.

*Tip:* Since this is a headless setup, you might want to place an empty file named "ssh" into the boot partition of the
SD-card to enable the ssh server!

For the official Raspberry Pi OS,you need to manually log in. The default user name is pi, with password raspberry.

Setup your freshly installed RaspberryPi:

Login to your server: ssh pi@raspberrypi, password "raspberry"

Rename your device to, as an example, "kasi":
    Become a root user: sudo -s or su -
    Edit the file /etc/hostname: vi /etc/hostname
        replace "raspberrypi" with your new hostname (i.e. "kasi")
    Edit the file /etc/hosts: vi /etc/hosts
        replace "raspberrypi" with your new hostname (i.e. "kasi")

    Run command: /etc/init.d/hostname.sh start


Now set up the user account for Kasi:
    sudo adduser kasi
        (password "Kasi")
    sudo usermod -a -G gpio,audio,video,cdrom,spi kasi


You should change the password of the "pi" user to something more secure:
    sudo passwd pi
        and then enter a good, new password.



Install software:
    sudo apt-get install vlc
    sudo apt-get install espeak
    sudo apt-get install espeakng

Enable SPI on Raspi: (https://pimylifeup.com/raspberry-pi-rfid-rc522/)
    "sudo raspi-config"

    In the setup menu select 5 (Interfacing Options)
    and enable SPI (P4 SPI)
    (A reboot might be in order)

    Install spidev library for python (pip3 install spidev)

    Install mfrc522 library for phyton (pip3 install mfrc522)


## Hardware
- RFID RC522 Reader module.
*Careful:* These RC522 modules sometimes have different pin usage!!
Connections (see Picture RFID_PINs.png)

|PI-PIN     |   MODULE-PIN |
|-----------|--------------|
| 1 (3.3v)  |    VCC/3v3 |
| 6 (GND)   |   GND |
| 19 (MOSI) |   MOSI |
| 21 (MISO) |   MISO |
| 22 (RST)  |   RST |
| 23 (SCK)  |   SCK |
| 24        |   NSS/SDA |


## Dependencies

### OS
The project is developed under Linux. It might well be portable to
Windows or Mac.

### Media Player
Currently vlc is being used as a media player, but mpd is being evaluated as 
another option.

### python modules / libraries
*This section is badly outdated and has to be reviewed!*
- gpio zero
- vlc / python-vlc
- pyttsx3
https://github.com/nateshmbhat/pyttsx3

### Other Software
- espeak-ng (deprecated?)
- espeak (for sstx3)


# Hardware

## Power
This project uses a standard USB power bank to avoid the hardware
related risks in such a project. A quality power bank is durable
and does not include the risk of fire in case of rough handling and you can swap 
out the power source without any code changes, as long as the Raspi recognises a 
low power situation. Additional drivers can be swapped in quite simply 
(including GPIO based power measuring).
The Raspi can pull power from the bank while being charged.
A 10kAh bank *should* be enough for ~3 hours of cable less music.

## NFC Reader
A USB NFC reader(/writer) is being evaluated for better album selection.

## Control
GPIO pins of a Raspberry Pi are the leading idea for controlling the box,
but control can be reconfigured or completely replaced via
the kasi_os.py file.

## Case
The case will be a wooden box with wooden buttons and without a
display.
An optional power/state LED is planned to be included.

# TODOS & IDEAS
## TODOs
- Hardware testing
- NFC evaluation
- Turn on WiFi by special button combo and launch of the browser based manager/alarm clock setup (see IDEAs).

## IDEAs
- Include a browser based alarm clock setup, including album/track selection as alarm sound.
- Include a (flask based?) browser based tool to manage albums and NFC IDs. It should allow album creation/deletion, file upload and album setup/editing: Removing/adding single tracks.
Maybe even remote control?

# Sources
- Color Resistor Chart
  https://www.digikey.com/en/resources/conversion-calculators/conversion-calculator-resistor-color-code-4-band
- BD677 Datasheet
  http://onsemi.com
- The great Unicode Search Tool
  http://www.fileformat.info/info/unicode/char/search.htm