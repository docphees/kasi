#!/bin/bash

# INSTALL DEPENDENCIES
# Arch:
# sudo pacman -S vlc 
# sudo pacman -S espeak 
# sudo pacman -S python-pip

sudo apt-get install vlc
sudo apt-get install espeak
sudo apt-get install python3-pip

sudo pip3 install python-vlc
sudo pip3 install RPi.GPIO
sudo pip3 install gpiozero
sudo pip3 install pyttsx3
sudo pip3 install spidev
sudo pip3 install mfrc522
